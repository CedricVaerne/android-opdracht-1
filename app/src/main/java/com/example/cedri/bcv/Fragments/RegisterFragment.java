package com.example.cedri.bcv.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cedri.bcv.Activities.MainActivity;
import com.example.cedri.bcv.DB.DatabaseHelper;
import com.example.cedri.bcv.Model.Profiel;
import com.example.cedri.bcv.R;


public class RegisterFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    View view;
    ImageView imgRegLog;
    TextView tvAlRegistered;
    EditText etUsername,etPassword,etConfPass;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_register, container, false);
        etUsername = (EditText)view.findViewById(R.id.txtNewUsername);
        etPassword = (EditText)view.findViewById(R.id.txtNewPassword);
        etConfPass = (EditText)view.findViewById(R.id.txtConfPassword);
        imgRegLog = (ImageView) view.findViewById(R.id.imgRegLog);

        tvAlRegistered = (TextView)view.findViewById(R.id.tvAlRegistered);
        tvAlRegistered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.moveToLoginFragment();
            }
        });
        imgRegLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etUsername.getText().toString().trim().equalsIgnoreCase(""))
                    etUsername.setError("De gebruikersnaam mag niet leeg zijn!");
                else {
                    if(etPassword.getText().toString().trim().equalsIgnoreCase(""))
                        etPassword.setError("Het passwoord mag niet leeg zijn!");
                    else {
                        if (etConfPass.getText().toString().trim().equalsIgnoreCase(""))
                            etConfPass.setError("Het Herhalende passwoord mag niet leeg zijn");
                        else {
                            //controle als username al bestaat
                            String check = mListener.getMyDB().getProfielname(etUsername.getText().toString());
                            System.out.println(check);
                            if(check == null)
                            {
                                if(etPassword.getText().toString().trim().equals(etConfPass.getText().toString().trim()))
                                {
                                    Profiel p = new Profiel(etUsername.getText().toString()+""+etPassword.getText().toString(),etUsername.getText().toString(),"","",etConfPass.getText().toString());
                                    mListener.getMyDB().insertProfiel(p.getId(),p.getUsername(), p.getPassword());
                                    System.out.println(mListener.getMyDB().insertProfiel(p.getId(),p.getUsername(), p.getPassword()));
                                    Intent intent = new Intent(getActivity(), MainActivity.class);
                                    intent.putExtra("profiel", p.toJson());
                                    startActivity(intent);
                                    getActivity().finish();
                                }
                                else{
                                    etConfPass.setError("Het passwoord is niet hetzelfde!");
                                }
                            }
                            else{
                                etUsername.setError("Deze gebruiksnaam bestaat al!");
                            }
                        }
                    }
                }
            }
        });
        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener test");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        public DatabaseHelper getMyDB();
        public void moveToLoginFragment();
    }
}
