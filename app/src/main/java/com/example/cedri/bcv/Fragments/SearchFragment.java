package com.example.cedri.bcv.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cedri.bcv.Adapter.BCVAdapter;
import com.example.cedri.bcv.DB.DatabaseHelper;
import com.example.cedri.bcv.Interface.ODBCVInterface;
import com.example.cedri.bcv.Model.BierCafe;
import com.example.cedri.bcv.R;
import com.example.cedri.bcv.Util.ODBBCWebservice;

import java.util.ArrayList;

/**
 * Created by cedri on 29/03/2017.
 */

public class SearchFragment extends Fragment implements ODBCVInterface {

    private View v;
    ListView lvBBC;
    ProgressBar progressBar;
    TextView no_internet_message;
    TextView waiting_message;
    private OnFragmentInteractionListener mListener;

    public SearchFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_search,container,false);

        lvBBC = (ListView)v.findViewById(R.id.lvBBC);

        progressBar = (ProgressBar)v.findViewById(R.id.progressBar);
        no_internet_message = (TextView)v.findViewById(R.id.no_internet_message);
        no_internet_message.setVisibility(View.GONE);
        waiting_message = (TextView)v.findViewById(R.id.waiting_message);
        waiting_message.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        waiting_message.setVisibility(View.VISIBLE);

        if(isOnline()){
            //roep de webservice op
            ODBBCWebservice ODBBCWebservice = new ODBBCWebservice();
            ODBBCWebservice.execute(this);
            waiting_message.setVisibility(View.GONE);
        }else{
            //Geen internet melding
            no_internet_message.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
        return v;
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(getContext().CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    public void updateScreenBVC(ArrayList<BierCafe> bierCafes) {
            progressBar.setVisibility(View.GONE);
            lvBBC.setAdapter(new BCVAdapter(this.getContext(),bierCafes));
            lvBBC.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    BierCafe e = (BierCafe) adapterView.getItemAtPosition(i);
                    mListener.moveToDetailFragment(e);
                }
            });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void moveToDetailFragment(BierCafe bc);
    }
}
