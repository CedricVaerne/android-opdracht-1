package com.example.cedri.bcv.Interface;

import com.example.cedri.bcv.Model.BierCafe;
import java.util.ArrayList;

/**
 * Created by cedri on 12/05/2016.
 */
public interface ODBCVInterface {
    public void updateScreenBVC(ArrayList<BierCafe> bierCafes);

}