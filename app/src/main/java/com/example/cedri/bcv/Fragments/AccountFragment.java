package com.example.cedri.bcv.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cedri.bcv.DB.DatabaseHelper;
import com.example.cedri.bcv.Model.Profiel;
import com.example.cedri.bcv.R;

/**
 * Created by cedri on 29/03/2017.
 */

public class AccountFragment extends Fragment {

    View v;
    Profiel p;
    String id;
    private OnFragmentInteractionListener mListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_account,container,false);

        p= mListener.getProfiel();
        id= p.getId();

        final EditText etUsername= (EditText)v.findViewById(R.id.etUsername);
        final EditText etEmail= (EditText)v.findViewById(R.id.etEmail);
        final EditText etFirstname= (EditText)v.findViewById(R.id.etFirstname);
        final EditText etCurrentPasswoord=(EditText) v.findViewById(R.id.ethuidigPassword);
        final EditText etPasswoord= (EditText)v.findViewById(R.id.etPasswoord);
        final EditText etHerhaalpasswoord= (EditText)v.findViewById(R.id.etHerhaalpasswoord);

        final TextView tvFoutmelding=(TextView) v.findViewById(R.id.tvFoutmelding);

        etUsername.setText(p.getUsername());
        if(p.getFirstname()!=null){
            etFirstname.setText(p.getFirstname());
        }
        if(p.getEmail()!=null){
            etEmail.setText((p.getEmail()));
        }

        Button btnSave = (Button)v.findViewById(R.id.btnsave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvFoutmelding.setText("");
                Boolean ok=true;
                System.out.println(p.getPassword());
                if(p.getPassword().equals(etCurrentPasswoord.getText().toString())){
                    if(etUsername.getText().toString().length()<3 || etUsername.getText().toString().trim().isEmpty()){
                        tvFoutmelding.setText(tvFoutmelding.getText()+"\n• Je nieuwe gebruikersnaam moet minimum 3 letters lang zijn.");
                        ok=false;
                    }else if(!etUsername.getText().toString().equals(p.getUsername()) && mListener.getMyDB().getProfiel(etUsername.getText().toString())!=null){
                        tvFoutmelding.setText(tvFoutmelding.getText()+"\n• Deze gebruiksnaam is al genomen.");
                        ok=false;
                    }
                    else
                    {
                        p.setUsername(etUsername.getText().toString());
                    }
                    if(etEmail.getText().toString().trim().isEmpty()){
                        tvFoutmelding.setText(tvFoutmelding.getText()+"\n• Je nieuwe emailadres mag niet leeg zijn.");
                        ok=false;
                    }else{
                        p.setEmail(etEmail.getText().toString());
                    }
                    if(etFirstname.getText().toString().trim().isEmpty()){
                        tvFoutmelding.setText(tvFoutmelding.getText()+"\n• Je nieuwe Voornaam mag niet leeg zijn.");
                        ok=false;
                    }else{
                        p.setFirstname(etFirstname.getText().toString());
                    }
                    if(etPasswoord.getText().toString().trim().equals(etHerhaalpasswoord.getText().toString().trim()) && etPasswoord.getText().toString().trim().length()>0){
                        p.setPassword(etPasswoord.getText().toString());
                    }else{
                        ok=false;
                        tvFoutmelding.setText(tvFoutmelding.getText()+"\n• Zorg dat de de nieuwe wachtwoorden hetzelfde zijn!");
                    }

                    if(ok){
                        mListener.savechanges(id,p);
                        //TOAST
                        Toast.makeText(getContext(),"Uw gegevens werden correct opgeslagen",Toast.LENGTH_LONG).show();
                        etPasswoord.setText("");
                        etHerhaalpasswoord.setText("");
                        etCurrentPasswoord.setText("");

                    }
                }else{
                    tvFoutmelding.setText("• U moet een Correct huidig wachtwoord ingeven!");

                }



            }
        });
        return v;
    } @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        Profiel getProfiel();
        void savechanges(String id,Profiel p);
        DatabaseHelper getMyDB();
    }
}
