package com.example.cedri.bcv.Model;

/**
 * Created by cedri on 29/03/2017.
 */

public class BierCafe {
    private String name;
    private String street;
    private String house_number;
    private String post_code;
    private String city_name;
    private String province;
    private String phone;
    private String email;
    private String website;
    private String description;
    private String openingstijden;
    private String extra;

    public BierCafe(String name, String street, String house_number, String post_code,
                    String city_name, String province, String phone, String email,
                    String website, String description, String openingstijden,String extra){
        this.name=name;
        this.street=street;
        this.house_number=house_number;
        this.post_code=post_code;
        this.city_name=city_name;
        this.province=province;
        this.phone=phone;
        this.email=email;
        this.website=website;
        this.description=description;
        this.openingstijden=openingstijden;
        this.extra=extra;
    }
    public BierCafe(){

    }

    public String getName(){
        return name;
    }
    public void setName(String n){
        name=n;
    }

    public String getStreet(){
        return street;
    }
    public void setStreet(String s){
        street=s;
    }

    public String getHouse_number(){
        return house_number;
    }
    public void setHouse_number(String n){
        house_number=n;
    }

    public String getPost_code(){
        return post_code;
    }
    public void setPost_code(String n){
        post_code=n;
    }

    public String getCity_name(){
        return city_name;
    }
    public void setCity_name(String n){
        city_name=n;
    }

    public String getProvince(){
        return province;
    }
    public void setProvince(String n){
        province=n;
    }

    public String getPhone(){
        return phone;
    }
    public void setPhone(String n){
        phone=n;
    }

    public String getEmail(){
        return email;
    }
    public void setEmail(String n){
        email=n;
    }

    public String getWebsite(){
        return website;
    }
    public void setWebsite(String n){
        website=n;
    }

    public String getDescription(){
        return description;
    }
    public void setDescription(String n){
        description=n;
    }

    public String getOpeningstijden(){
        return openingstijden;
    }
    public void setOpeningstijden(String n){
        openingstijden=n;
    }

    public String getExtra(){
        return extra;
    }
    public void setExtra(String n){
        extra=n;
    }

}
