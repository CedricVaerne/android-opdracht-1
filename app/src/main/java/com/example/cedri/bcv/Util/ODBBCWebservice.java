package com.example.cedri.bcv.Util;

import android.os.AsyncTask;


import com.example.cedri.bcv.Interface.ODBCVInterface;
import com.example.cedri.bcv.Model.BierCafe;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;


public class ODBBCWebservice extends AsyncTask<ODBCVInterface, Void, String> {

	private ODBCVInterface listener;

	@Override
	protected String doInBackground(ODBCVInterface... arg0) {
		listener = arg0[0];
		
		StringBuilder newsBuilder = new StringBuilder();

			// execute search
			HttpClient newsClient = new DefaultHttpClient();
			try {
                HttpGet newsGet = new HttpGet("http://opendata.visitflanders.org/tourist/reca/beer_bars.xml");
				HttpResponse newsResponse = newsClient.execute(newsGet);
				StatusLine newsSearchStatus = newsResponse
						.getStatusLine();

				if (newsSearchStatus.getStatusCode() == 200) {
					// we have an OK response
					HttpEntity newsEntity = newsResponse.getEntity();
					InputStream placesContent = newsEntity.getContent();
					InputStreamReader placesInput = new InputStreamReader(
							placesContent);
					BufferedReader placesReader = new BufferedReader(
							placesInput);

					String lineIn;
					while ((lineIn = placesReader.readLine()) != null) {
						newsBuilder.append(lineIn);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		return newsBuilder.toString();
	}

	protected void onPostExecute(String result) {
		ArrayList<BierCafe> bierCafes = processResult(result);
		listener.updateScreenBVC(bierCafes);
	}
	
	private ArrayList<BierCafe> processResult(String result){
		ArrayList<BierCafe> bierCafes = new ArrayList<BierCafe>();
		BierCafe bierCafe= new BierCafe();
		try {
			// parse XML
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();
			xpp.setInput(new StringReader(result));

			int eventType = xpp.getEventType();
			boolean isItem = false;
			//we gaan elke tag zoeken en koppelen aan onze BierCafe klasse , aan het specifieke veld
			while (eventType != XmlPullParser.END_DOCUMENT) {
				if (eventType == XmlPullParser.START_TAG) {
					if (xpp.getName().equalsIgnoreCase("element")){
						bierCafe= new BierCafe();
						isItem=true;
					} else if (xpp.getName().equalsIgnoreCase("Name") && isItem){
						xpp.next();
						bierCafe.setName(xpp.getText());
					} else if (xpp.getName().equalsIgnoreCase("Street") && isItem){
						xpp.next();
						bierCafe.setStreet(xpp.getText());
					} else if (xpp.getName().equalsIgnoreCase("House_number") && isItem){
						xpp.next();
						bierCafe.setHouse_number(xpp.getText());}
					else if(xpp.getName().equalsIgnoreCase("postal_code") && isItem){
							xpp.next();
						bierCafe.setPost_code(xpp.getText());}
					else if(xpp.getName().equalsIgnoreCase("city_name") && isItem) {
						xpp.next();
						bierCafe.setCity_name(xpp.getText());
					}
					else if(xpp.getName().equalsIgnoreCase("Province") && isItem){
						xpp.next();
						bierCafe.setProvince(xpp.getText());
					}
					else if(xpp.getName().equalsIgnoreCase("phone") && isItem) {
						xpp.next();
						bierCafe.setPhone(xpp.getText());
					}
					else if(xpp.getName().equalsIgnoreCase("Email") && isItem) {
						xpp.next();
						bierCafe.setEmail(xpp.getText());
					}
					else if(xpp.getName().equalsIgnoreCase("Website") && isItem) {
						xpp.next();
						bierCafe.setWebsite(xpp.getText());
					}
					else if(xpp.getName().equalsIgnoreCase("description_nl") && isItem) {
						xpp.next();
						bierCafe.setDescription(xpp.getText());
					}
					else if(xpp.getName().equalsIgnoreCase("Opening_times_nl") && isItem) {
						xpp.next();
						bierCafe.setOpeningstijden(xpp.getText());
					}
					else if(xpp.getName().equalsIgnoreCase("Group_access_nl") && isItem) {
						xpp.next();
						bierCafe.setExtra(xpp.getText());

						bierCafes.add(bierCafe);
						isItem=false;
					}
				}
				eventType = xpp.next();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return bierCafes;
	}
}
