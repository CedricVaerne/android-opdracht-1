package com.example.cedri.bcv.Model;

import com.google.gson.Gson;

/**
 * Created by cedri on 14/09/2016.
 */
public class Profiel {

    String id;
    String username;
    String firstname;
    String email;
    String password;

    public Profiel(String id,String username, String firstname, String email, String password) {
        this.id = id;
        this.username = username;
        this.firstname = firstname;
        this.email = email;
        this.password = password;
    }

    public Profiel()
    {}

    public String getId(){
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static Profiel fromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Profiel.class);
    }

    public String toString(){
        return getUsername() + " " + getPassword();
    }
}
