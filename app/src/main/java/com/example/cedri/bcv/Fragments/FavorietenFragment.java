package com.example.cedri.bcv.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.cedri.bcv.Adapter.BCVAdapter;
import com.example.cedri.bcv.DB.DatabaseHelper;
import com.example.cedri.bcv.Model.BierCafe;
import com.example.cedri.bcv.R;

import java.util.ArrayList;


public class FavorietenFragment extends Fragment {


    LinearLayout llNoFavo;
    ListView lvfavorieten;
    View v;
    private OnFragmentInteractionListener mListener;

    public FavorietenFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_favorieten, container, false);
        lvfavorieten=(ListView)v.findViewById(R.id.lvfav);
        llNoFavo = (LinearLayout)v.findViewById(R.id.llNoFavo);
        if(mListener.getBierCafes()!=null){
            //toon alle favorieten van het ingelogde account
            llNoFavo.setVisibility(View.GONE);
            lvfavorieten.setAdapter(new BCVAdapter(this.getContext(),mListener.getBierCafes()));
        }
        else{
            //Bij een lege favorietenlijst
            llNoFavo.setVisibility(View.VISIBLE);
        }
        lvfavorieten.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //ga naar de detailpagina van de geselcteerde favoriet
                BierCafe bc = (BierCafe) adapterView.getItemAtPosition(i);
                mListener.moveToDetailFragment(bc);
            }
        });
        return v;
    }

  @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public ArrayList getBierCafes();
        public void moveToDetailFragment(BierCafe bc);
    }
}
