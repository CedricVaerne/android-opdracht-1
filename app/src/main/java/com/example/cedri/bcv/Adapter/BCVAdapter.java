package com.example.cedri.bcv.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cedri.bcv.DB.DatabaseHelper;
import com.example.cedri.bcv.Model.BierCafe;
import com.example.cedri.bcv.R;

import java.util.ArrayList;

/**
 * Created by cedri on 30/11/2016.
 */

public class BCVAdapter extends BaseAdapter {
    Context mContext;
    ArrayList<BierCafe> bierCafes;
    LayoutInflater mInflater;

    public BCVAdapter(Context mContext, ArrayList<BierCafe> bcafes) {
        this.mContext = mContext;
        bierCafes = bcafes;
        try {
            mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }catch (Exception e){
            e.getMessage();
        }
    }

    @Override
    public int getCount() {
        return bierCafes.size();
    }

    @Override
    public Object getItem(int i) {
        return bierCafes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view1= mInflater.inflate(R.layout.cafe_view_item,null);
        TextView tvNaam= (TextView) view1.findViewById(R.id.tvNaam);
        TextView tvPostcode= (TextView) view1.findViewById(R.id.tvPostcode);
        TextView tvPlaats= (TextView) view1.findViewById(R.id.tvPlaats);
        ImageView imgstar=(ImageView)view1.findViewById(R.id.imgstar);



        BierCafe bierCafe = bierCafes.get(i);


        tvNaam.setText(bierCafe.getName());
        tvPlaats.setText(bierCafe.getCity_name());
        tvPostcode.setText(bierCafe.getPost_code()+" ");
        return view1;
    }
}
