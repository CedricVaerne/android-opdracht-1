package com.example.cedri.bcv.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.cedri.bcv.DB.DatabaseHelper;
import com.example.cedri.bcv.Model.BierCafe;
import com.example.cedri.bcv.Model.Profiel;
import com.example.cedri.bcv.R;

import java.net.URL;

/**
 * Created by cedri on 6/12/2016.
 */
public class BierCafeDetailFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    View view;
    String titel,description,email,location,plaats,phonenumber;
    TextView txtphonenumber,txtemail,txtwebsite,txtProvincie,txtBeschrijving,txtLocation;
    Toolbar tTitel;
    FloatingActionButton fabAdd,fabRem;

    public BierCafeDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_biercafedetails, container, false);
        final BierCafe e = mListener.getBierCafe();
        titel = e.getName();
        description = e.getDescription();
        email = e.getEmail();
        final String website = e.getWebsite();
        phonenumber = e.getPhone();
        location = e.getCity_name();
        plaats = e.getProvince();

        if(phonenumber==null){
            LinearLayout lphone=(LinearLayout)view.findViewById(R.id.cPhone);
            lphone.setVisibility(View.GONE);
        }if(email==null){
            LinearLayout lEmail=(LinearLayout)view.findViewById(R.id.cEmail);
            lEmail.setVisibility(View.GONE);
        }if(website==null){
            LinearLayout lWebsite=(LinearLayout)view.findViewById(R.id.cWebsite);
            lWebsite.setVisibility(View.GONE);
        }if(location==null){
            LinearLayout lLocation=(LinearLayout)view.findViewById(R.id.cLocation);
            lLocation.setVisibility(View.GONE);
        }

        tTitel = (Toolbar)view.findViewById(R.id.tvFragTitel);
        tTitel.setTitle(titel);

        txtphonenumber = (TextView)view.findViewById(R.id.tvFragPhone);
        txtphonenumber.setText(phonenumber);


        txtemail = (TextView)view.findViewById(R.id.tvFragEmail);
        txtemail.setText(email);

        txtwebsite = (TextView)view.findViewById(R.id.tvFragWeb);
        txtwebsite.setText(website);
        txtwebsite.setTextColor(Color.BLUE);
        txtwebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToUrl(website);
            }
        });

        txtProvincie= (TextView)view.findViewById(R.id.tvFragprovincie);
        txtProvincie.setText(plaats);

        txtBeschrijving= (TextView)view.findViewById(R.id.tvFragBeschrijving);
        txtBeschrijving.setText(description);

        txtLocation = (TextView)view.findViewById(R.id.tvFragLocation);
        txtLocation.setText(location);

        fabAdd = (FloatingActionButton) view.findViewById(R.id.fabAdd);
        fabRem = (FloatingActionButton) view.findViewById(R.id.fabRem);


        //Controle of het biercafe al bij de favorieten zit van het account
        if(mListener.getMyDB().isBierCafeexist(mListener.getBierCafe(),mListener.getProfiel().getId()) )
        {
            //verwijderen van favorieten fab knop activeren
            fabAdd.setVisibility(View.INVISIBLE);
            fabAdd.setEnabled(false);
            fabRem.setVisibility(View.VISIBLE);
            fabRem.setEnabled(true);
            fabRem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Verwijderd van favorieten", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    mListener.getMyDB().deleteBiercafe(mListener.getBierCafe().getName());
                    mListener.moveToFavouritesFragment();
                }
            });
        }
        else {
            fabRem.setVisibility(View.INVISIBLE);
            fabRem.setEnabled(false);
            fabAdd.setVisibility(View.VISIBLE);
            fabAdd.setEnabled(true);
            fabAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Toegevoegd aan favorieten", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    mListener.getMyDB().insertBiercafe(mListener.getBierCafe(),mListener.getProfiel().getId());
                    mListener.moveToFavouritesFragment();
                }
            });
        }
        return view;
    }

    private void goToUrl (String url) {
        Uri uriUrl = Uri.parse("http://"+url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        DatabaseHelper getMyDB();
        BierCafe getBierCafe();
        Profiel getProfiel();
        void moveToFavouritesFragment();
    }
}
