
package com.example.cedri.bcv.Activities;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.cedri.bcv.DB.DatabaseHelper;
import com.example.cedri.bcv.Fragments.LoginFragment;
import com.example.cedri.bcv.Fragments.RegisterFragment;
import com.example.cedri.bcv.Model.Profiel;


public class LoginActivity extends AppCompatActivity implements
        LoginFragment.OnFragmentInteractionListener, RegisterFragment.OnFragmentInteractionListener {

    DatabaseHelper myDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myDB = DatabaseHelper.getInstance(this);
        getFragmentManager().beginTransaction().add(android.R.id.content, new LoginFragment()).addToBackStack("Login").commit();
    }

    @Override
    public void moveToRegisterFragment() {
        getFragmentManager().beginTransaction().replace(android.R.id.content,new RegisterFragment()).addToBackStack("Register").commit();
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public DatabaseHelper getMyDB() {
        return myDB;
    }

    @Override
    public void moveToLoginFragment() {
        getFragmentManager().beginTransaction().replace(android.R.id.content,new LoginFragment()).addToBackStack("Login").commit();
    }

}
