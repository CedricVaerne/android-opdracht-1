package com.example.cedri.bcv.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cedri.bcv.Activities.MainActivity;
import com.example.cedri.bcv.DB.DatabaseHelper;
import com.example.cedri.bcv.Model.Profiel;
import com.example.cedri.bcv.R;


public class LoginFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        final EditText tvUsername = (EditText)view.findViewById(R.id.txtUsername);
        final EditText tvPassword = (EditText)view.findViewById(R.id.txtPassword);
        ImageView bLogin = (ImageView) view.findViewById(R.id.imgLogin);
        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tvUsername.getText().toString().trim().equalsIgnoreCase(""))
                    tvUsername.setError("Dit veld mag niet leeg zijn!");
                else {
                    if(tvPassword.getText().toString().trim().equalsIgnoreCase(""))
                        tvPassword.setError("Dit veld mag niet leeg zijn!");
                    else {
                        Profiel p = mListener.getMyDB().getProfiel(tvUsername.getText().toString());
                        if(p == null) {
                            Snackbar.make(v, "Deze gebruikersnaam bestaat niet!", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                        else{
                            System.out.println(p.getPassword());
                            if(p.getPassword().equals(tvPassword.getText().toString().trim()))
                            {
                                System.out.println("DIT IS HET ID:"+p.getId());
                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                intent.putExtra("profiel", p.toJson());
                                startActivity(intent);
                                getActivity().finish();
                            }
                            else
                            {
                                tvPassword.setError("Paswoord is foutief");
                            }
                        }
                    }
                }
            }
        });
        TextView bRegister = (TextView)view.findViewById(R.id.btnRegister);
        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.moveToRegisterFragment();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void moveToRegisterFragment();
        public DatabaseHelper getMyDB();
    }
}
