package com.example.cedri.bcv.Activities;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.cedri.bcv.DB.DatabaseHelper;
import com.example.cedri.bcv.Fragments.AccountFragment;
import com.example.cedri.bcv.Fragments.FavorietenFragment;
import com.example.cedri.bcv.Fragments.BierCafeDetailFragment;
import com.example.cedri.bcv.Fragments.SearchFragment;
import com.example.cedri.bcv.Model.BierCafe;
import com.example.cedri.bcv.Model.Profiel;
import com.example.cedri.bcv.R;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements AccountFragment.OnFragmentInteractionListener,BierCafeDetailFragment.OnFragmentInteractionListener,SearchFragment.OnFragmentInteractionListener,FavorietenFragment.OnFragmentInteractionListener, NavigationView.OnNavigationItemSelectedListener{

    DatabaseHelper myDB;
    private Boolean eventOpened;
    BierCafe bierCafe;
    Profiel p;

    View parentLayout;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        getFragmentManager().beginTransaction().replace(R.id.content_frame, new FavorietenFragment()).addToBackStack("Begin").commit();

        //getFragmentManager().beginTransaction().add(android.R.id.content, new FavorietenFragment()).addToBackStack("Begin").commit();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //get profiel
        Bundle bundle = getIntent().getExtras();
        p = p.fromJson(bundle.getString("profiel"));



        //initialize DB
        myDB = DatabaseHelper.getInstance(this);

        eventOpened = false;

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

    }

    @Override
    public DatabaseHelper getMyDB() {
        return myDB;
    }

    @Override
    public BierCafe getBierCafe() {  return bierCafe;}

    @Override
    public Profiel getProfiel() {
        System.out.println(p.getUsername());
        return p;
    }

    @Override
    public void savechanges(String id, Profiel p) {
        System.out.println("DIT IS HET ID bij savechanges:"+id);
        //Profiel n = myDB.getProfielWid(id);
        //System.out.println(n.getId());
        myDB.changeprofiel(id,p);
    }

    @Override
    public void moveToFavouritesFragment() {
        System.out.println("DIT IS HET ID bij favo:"+p.getId());
        getFragmentManager().beginTransaction().replace(R.id.content_frame, new FavorietenFragment()).addToBackStack("Begin").commit();

        navigationView.getMenu().getItem(0).setChecked(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(eventOpened) {
                eventOpened = false;
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
            else{
                if (getFragmentManager().getBackStackEntryCount() == 0) {
                    super.onBackPressed();
                } else {
                    getFragmentManager().popBackStack();
                }
            }
        }

        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("App sluiten")
                .setMessage("Bent u zeker om de app te sluiten?")
                .setPositiveButton("Ja", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("Nee", null)
                .show();

    }
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        eventOpened = false;
        if (id == R.id.nav_fav) {
            moveToFavouritesFragment();
            //getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else if (id == R.id.nav_zoeken) {
            moveToSearchFragment();

        } else if (id == R.id.nav_account) {
            moveToAccountFragment();

        } else if (id == R.id.nav_logout) {
            moveToLoginFragement();
    }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void moveToLoginFragement(){
        System.out.println("DIT IS HET ID bij login:"+p.getId());
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        this.finish();
    }
    public void moveToSearchFragment(){
        System.out.println("DIT IS HET ID bij search:"+p.getId());
        getFragmentManager().beginTransaction().replace(R.id.content_frame, new SearchFragment()).addToBackStack("Search").commit();
        navigationView.getMenu().getItem(1).setChecked(true);

    }

    public void moveToAccountFragment(){
        System.out.println("DIT IS HET ID bij account:"+p.getId());
        getFragmentManager().beginTransaction().replace(R.id.content_frame, new AccountFragment()).addToBackStack("Account").commit();
        navigationView.getMenu().getItem(2).setChecked(true);
    }


    @Override
    public void moveToDetailFragment(BierCafe bc) {
        System.out.println("DIT IS HET ID bij detail:"+p.getId());
        bierCafe=bc;
        getFragmentManager().beginTransaction().replace(R.id.content_frame, new BierCafeDetailFragment()).addToBackStack("BierCafeDetail").commit();
    }
    @Override
    public ArrayList getBierCafes() {
        System.out.println("DIT IS HET ID bij zoekenarraylist:"+p.getId());
        return myDB.getBierCafes(p.getId());
    }
}
