package com.example.cedri.bcv.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.cedri.bcv.Model.BierCafe;
import com.example.cedri.bcv.Model.Profiel;

import java.util.ArrayList;


/**
 * Created by cedri on 14/09/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper instance;

    //Database Versie
    public static final int database_version = 4;

    //Database Naam
    public static final String DATABASE_NAME = "Biercafevlaanderen.db";

    //Tabel Namen
    public static final String TABLE_NAME_PROFIELEN = "profielen";
    public static final String TABLE_NAME_BIERCAFES = "biercafes";

    //Kollom namen
    // voor tabel profiles
    public static String COL_1_PROFIELEN= "profile_id";
    public static final String COL_2_PROFIELEN= "username";
    public static final String COL_3_PROFIELEN = "firstname";
    public static final String COL_4_PROFIELEN = "email";
    public static final String COL_5_PROFIELEN = "password";

    //voor tabel
    public static final String COL_1_BIERCAFES = "biercafe_id";
    public static final String COL_2_BIERCAFES = "biercafe_name";
    public static final String COL_3_BIERCAFES = "biercafe_postcode";
    public static final String COL_4_BIERCAFES = "biercafe_plaats";
    public static final String COL_5_BIERCAFES = "biercafe_gemeente";
    public static final String COL_6_BIERCAFES = "biercafe_omschrijving";
    public static final String COL_7_BIERCAFES = "biercafe_phone";
    public static final String COL_8_BIERCAFES = "biercafe_website";
    public static final String COL_9_BIERCAFES = "biercafe_email";
    public static final String COL_10_BIERCAFES = "profielid";


    public static synchronized DatabaseHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseHelper(context.getApplicationContext());
        }
        return instance;
    }

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, database_version);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    public void changeprofiel(String id, Profiel p){

        SQLiteDatabase db = this.getWritableDatabase();


       /* ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2_PROFIELEN, p.getUsername());
        contentValues.put(COL_3_PROFIELEN, p.getFirstname());
        contentValues.put(COL_4_PROFIELEN, p.getEmail());
        contentValues.put(COL_5_PROFIELEN, p.getPassword());*/

        // de insert methode geeft -1 terug als het niet gelukt is en de row value als het wel gelukt is

        System.out.println(getProfielWid(id).getId());
    //    db.update(TABLE_NAME_PROFIELEN, contentValues,COL_1_PROFIELEN ="'" + id +"'",null);


        db.execSQL("UPDATE "+ TABLE_NAME_PROFIELEN + " SET "+COL_2_PROFIELEN+" = '"+p.getUsername()+"', " +COL_3_PROFIELEN+" = '"+p.getFirstname()+"', "+COL_4_PROFIELEN+" = '"+p.getEmail()+"', "+COL_5_PROFIELEN+" = '"+p.getPassword()+
                "' WHERE " + COL_1_PROFIELEN + "='" + id + "'");

    }

    public Profiel getProfiel(String profielname){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_NAME_PROFIELEN + " WHERE " + COL_2_PROFIELEN + "='" + profielname + "'", null);
        if(res.getCount() == 0)
        {
            return null;
        }
        else{
            Profiel p = null;
            while (res.moveToNext())
            {
                p = new Profiel(res.getString(0),res.getString(1),res.getString(2),res.getString(3),res.getString(4));
            }

            return p;
        }
    }

    public Profiel getProfielWid(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_NAME_PROFIELEN + " WHERE " + COL_1_PROFIELEN + "='" + id + "'", null);
        if(res.getCount() == 0)
        {
            return null;
        }
        else{
            Profiel p = null;
            while (res.moveToNext())
            {
                p = new Profiel(res.getString(0),res.getString(1),res.getString(2),res.getString(3),res.getString(4));
            }

            return p;
        }
    }

    public String getProfielname(String profielname){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_NAME_PROFIELEN + " WHERE " + COL_2_PROFIELEN + "='" + profielname + "'", null);
        if(res.getCount() == 0)
        {
            return null;
        }
        else {
            Profiel p = null;
            while (res.moveToNext()) {
                p = new Profiel(res.getString(0), res.getString(1), res.getString(2), res.getString(3), res.getString(4));
            }

            return p.getUsername();
        }
    }

    public int insertProfiel (String id,String username, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1_PROFIELEN,id);
        contentValues.put(COL_2_PROFIELEN, username);
        contentValues.put(COL_5_PROFIELEN, password);

        // de insert methode geeft -1 terug als het niet gelukt is en de row value als het wel gelukt is
        long result = db.insert(TABLE_NAME_PROFIELEN, null, contentValues);

        return (int)result;
    }

    public int insertBiercafe (BierCafe bierCafe, String profileid) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2_BIERCAFES, bierCafe.getName());
        contentValues.put(COL_3_BIERCAFES, bierCafe.getPost_code());
        contentValues.put(COL_4_BIERCAFES, bierCafe.getProvince());
        contentValues.put(COL_5_BIERCAFES, bierCafe.getCity_name());
        contentValues.put(COL_6_BIERCAFES, bierCafe.getDescription());
        contentValues.put(COL_7_BIERCAFES, bierCafe.getPhone());
        contentValues.put(COL_8_BIERCAFES, bierCafe.getWebsite());
        contentValues.put(COL_9_BIERCAFES, bierCafe.getEmail());
        contentValues.put(COL_10_BIERCAFES, profileid);

        // de insert methode geeft -1 terug als het niet gelukt is en de row value als het wel gelukt is
        long result = db.insert(TABLE_NAME_BIERCAFES, null, contentValues);

        return (int)result;
    }

    public Integer deleteBiercafe(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME_BIERCAFES, "biercafe_name = ?", new String[] {name});
    }

    public ArrayList<BierCafe> getBierCafes(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<BierCafe> favourites = new ArrayList<>();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_NAME_BIERCAFES + " WHERE " + COL_10_BIERCAFES + "='" + id + "'", null);
        if(res.getCount() == 0)
        {
            return null;
        }
        else {
            while (res.moveToNext()) {
                BierCafe bc = new BierCafe(res.getString(1),null,null, res.getString(2), res.getString(4), res.getString(3), res.getString(6), res.getString(8), res.getString(7),res.getString(5),null,null);
                favourites.add(bc);
            }

            return favourites;
        }
    }

    public Boolean isBierCafeexist(BierCafe bierCafe, String id){
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_NAME_BIERCAFES + " WHERE " + COL_10_BIERCAFES + "='" + id + "' AND " + COL_2_BIERCAFES + "='" + bierCafe.getName() + "'", null);
        if(res.getCount() == 0)
        {
            return false;
        }
        else {
            return true;
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // de tabel creëren voor de profielen
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME_PROFIELEN + " (" + COL_1_PROFIELEN + " VARCHAR(255), " + COL_2_PROFIELEN + " VARCHAR(255), " + COL_3_PROFIELEN + " VARCHAR(255), " + COL_4_PROFIELEN + " VARCHAR(255), " + COL_5_PROFIELEN + " VARCHAR(255))");

        // de tabel creëren voor de locaties met foreign key profile_id
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME_BIERCAFES + " (" + COL_1_BIERCAFES + " INTEGER PRIMARY KEY, " + COL_2_BIERCAFES + " VARCHAR(255), " + COL_3_BIERCAFES + " VARCHAR(255), " + COL_4_BIERCAFES + " VARCHAR(255), " + COL_5_BIERCAFES + " VARCHAR(255), " + COL_6_BIERCAFES +
                " VARCHAR(255), " + COL_7_BIERCAFES + " VARCHAR(255), " + COL_8_BIERCAFES + " VARCHAR(255), " + COL_9_BIERCAFES + " VARCHAR(255), " + COL_10_BIERCAFES + " VARCHAR(255), FOREIGN KEY (" + COL_10_BIERCAFES + ") REFERENCES " + TABLE_NAME_PROFIELEN + "(" + COL_1_PROFIELEN + "))");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_PROFIELEN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_BIERCAFES);
        onCreate(db);
    }
}
